#!/bin/bash

set -e

systemctl --user stop bhdouglass.indicatorweather.service
systemctl --user disable bhdouglass.indicatorweather.service

rm /home/phablet/.config/systemd/user/bhdouglass.indicatorweather.service
rm /home/phablet/.local/share/ayatana/indicators/bhdouglass.indicatorweather.indicator

echo "indicator weather uninstalled"
