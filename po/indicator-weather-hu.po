# Hungarian translation for ut-indicator-weather
# Copyright (c) 2017 Rosetta Contributors and Canonical Ltd 2017
# This file is distributed under the same license as the ut-indicator-weather package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: ut-indicator-weather\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2017-01-04 20:24+0000\n"
"PO-Revision-Date: 2017-01-04 08:31+0000\n"
"Last-Translator: Richard Somlói <ricsipontaz@gmail.com>\n"
"Language-Team: Hungarian <hu@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2017-01-08 04:28+0000\n"
"X-Generator: Launchpad (build 18302)\n"

#: ../qml/WeatherProviderSelect.qml:19
msgid "Weather Provider"
msgstr "Időjárási adatszolgáltató"

#: ../qml/WeatherProviderSelect.qml:41
msgid "Pirate Weather"
msgstr "Pirate Weather"

#: ../qml/WeatherProviderSelect.qml:68
msgid "OpenWeatherMap"
msgstr "OpenWeatherMap"

#: ../qml/Main.qml:25 ../indicator/bhdouglass-indicator-weather.py:294
msgid "Please specify an api key"
msgstr "Adja meg az API kulcsot"

#: ../qml/Main.qml:29 ../indicator/bhdouglass-indicator-weather.py:297
msgid "Please specify the latitude"
msgstr "Adja meg a szélességet"

#: ../qml/Main.qml:33 ../indicator/bhdouglass-indicator-weather.py:300
msgid "Please specify the longitude"
msgstr "Adja meg a hosszúságot"

#: ../qml/Main.qml:37
msgid "Saved the settings, please reboot"
msgstr "A beállítások elmentve, indítsa újra az eszközt"

#: ../qml/Main.qml:42
msgid "Failed to save the settings"
msgstr "Nem sikerült a beállítások mentése"

#: ../qml/Main.qml:51
msgid "Indicator Weather"
msgstr "Időjárás Indikátor"

#: ../qml/Main.qml:99
msgid "Pirate Weather API Key"
msgstr "Pirate Weather API kulcs"

#: ../qml/Main.qml:115
msgid "OpenWeatherMap API Key"
msgstr "OpenWeatherMap API kulcs"

#: ../qml/Main.qml:131
msgid "Click to signup for an API key"
msgstr "Kattintson az API kulcs beszerzéseséhez"

#: ../qml/Main.qml:145
msgid "Latitude"
msgstr "Szélesség"

#: ../qml/Main.qml:159
msgid "Longitude"
msgstr "Hosszúság"

#: ../qml/Main.qml:186
msgid "Save"
msgstr "Mentés"

#: ../qml/Main.qml:197
msgid "Install Indicator"
msgstr "Indikátor telepítése"

#: ../qml/Main.qml:208
msgid "Uninstall Indicator"
msgstr "Indikátor eltávolítása"

#: ../qml/Main.qml:229
msgid "Successfully installed, please reboot"
msgstr "Sikeres telepítés, indítsa újra az eszközt"

#: ../qml/Main.qml:233
msgid "Failed to install"
msgstr "Nem sikerült a telepítés"

#: ../qml/Main.qml:241
msgid "Successfully uninstalled, please reboot"
msgstr "Sikeres eltávolítás, indítsa újra az eszközt"

#: ../qml/Main.qml:245
msgid "Failed to uninstall"
msgstr "Sikertelen eltávolítás"

#: ../qml/TemperatureUnitSelect.qml:19
msgid "Temperature Unit"
msgstr "Hőmérsékleti egység"

#: ../qml/TemperatureUnitSelect.qml:42
msgid "Fahrenheit"
msgstr "Fahrenheit"

#: ../qml/TemperatureUnitSelect.qml:70
msgid "Celsius"
msgstr "Celsius"

#: ../qml/TemperatureUnitSelect.qml:98
msgid "Kelvin"
msgstr "Kelvin"

#: ../indicator/bhdouglass-indicator-weather.py:131
#: ../indicator/bhdouglass-indicator-weather.py:132
#: ../indicator/bhdouglass-indicator-weather.py:150
msgid "Clear"
msgstr "Tiszta"

#: ../indicator/bhdouglass-indicator-weather.py:133
#: ../indicator/bhdouglass-indicator-weather.py:156
#: ../indicator/bhdouglass-indicator-weather.py:158
msgid "Rainy"
msgstr "Esős"

#: ../indicator/bhdouglass-indicator-weather.py:134
#: ../indicator/bhdouglass-indicator-weather.py:162
msgid "Snowy"
msgstr "Hó"

#: ../indicator/bhdouglass-indicator-weather.py:135
msgid "Sleet"
msgstr "Ónos eső"

#: ../indicator/bhdouglass-indicator-weather.py:136
#: ../indicator/bhdouglass-indicator-weather.py:166
#: ../indicator/bhdouglass-indicator-weather.py:168
msgid "Windy"
msgstr "Szeles"

#: ../indicator/bhdouglass-indicator-weather.py:137
#: ../indicator/bhdouglass-indicator-weather.py:154
msgid "Foggy"
msgstr "Ködös"

#: ../indicator/bhdouglass-indicator-weather.py:138
#: ../indicator/bhdouglass-indicator-weather.py:139
#: ../indicator/bhdouglass-indicator-weather.py:140
#: ../indicator/bhdouglass-indicator-weather.py:152
msgid "Cloudy"
msgstr "Felhős"

#: ../indicator/bhdouglass-indicator-weather.py:141
#: ../indicator/bhdouglass-indicator-weather.py:164
msgid "Hail"
msgstr "Jégeső"

#: ../indicator/bhdouglass-indicator-weather.py:142
#: ../indicator/bhdouglass-indicator-weather.py:160
msgid "Stormy"
msgstr "Viharos"

#: ../indicator/bhdouglass-indicator-weather.py:143
#: ../indicator/bhdouglass-indicator-weather.py:170
msgid "Tornado"
msgstr "Tornádó"

#: ../indicator/bhdouglass-indicator-weather.py:172
msgid "Hurricane"
msgstr "Hurrikán"

#: ../indicator/bhdouglass-indicator-weather.py:174
msgid "Extreme Cold"
msgstr "Extrém hideg"

#: ../indicator/bhdouglass-indicator-weather.py:176
msgid "Extreme Heat"
msgstr "Extrém hőség"

#: ../indicator/bhdouglass-indicator-weather.py:263
msgid "Forecast"
msgstr "Előrejelzés"

#: ../indicator/bhdouglass-indicator-weather.py:266
msgid "Weather Settings"
msgstr "Időjárás beállítások"

#: ../indicator/bhdouglass-indicator-weather.py:313
#: ../indicator/bhdouglass-indicator-weather.py:322
#: ../indicator/bhdouglass-indicator-weather.py:335
#: ../indicator/bhdouglass-indicator-weather.py:339
#: ../indicator/bhdouglass-indicator-weather.py:354
#: ../indicator/bhdouglass-indicator-weather.py:363
#: ../indicator/bhdouglass-indicator-weather.py:374
#: ../indicator/bhdouglass-indicator-weather.py:378
msgid "Error fetching weather"
msgstr "Nem sikerült az időjárásadatok letöltése"

#. TRANSLATORS You must ensure that both {condition} and {temperature} remain intact as they are used for replacing with the actual values. For example: Clear and 75°
#: ../indicator/bhdouglass-indicator-weather.py:434
#, python-brace-format
msgid "{condition} and {temperature}°"
msgstr "{condition} és {temperature}°"
